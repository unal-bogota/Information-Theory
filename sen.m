 clc;
clear;
close all;
figure(1)
        
        f = [1 0 0 ]; %Frecuencia para contruccion funcion Original
        a = [1 0 0 ];    %Amplitud, solo se usa a(1)
        f(1)=600;
        
        lim = 4/f(1); %Lim del tiempo de graficacion
        
        ns = 0:1*10^(-4):lim; %Definicion de la linea de tiempo
        y = a(1)*sin( 2*f(1)*pi*ns ); %Funcion Original
       
        
            %Grafica de Funcion de Original
            subplot(221)
            hold on
            grid on
                %%Propiedades de la gráfica
                xlabel('t (seconds)','FontName','Arial','FontSize', 12);
                ylabel('Amplitud','FontName','Arial','FontSize', 12);
                title('Sen( 2* pi*f*t)','FontName','Arial','FontSize', 16);
                legend('real');
                plot(ns,y,'g');
                axis([0 lim -1.5 1.5 ]);
                
    num=4;
    s = num*f(1);
    disp(s)
    T=1/s;
    
    n = (0:T:lim)';
    ys=a(1)*sin(2*f(1)*pi*n); %muestreo de la señal
    
        %Grafica de Puntos Muestrales
        subplot(222)
        %%Propiedades de la gráfica
        stem(n,ys,'or');grid
        axis([0 lim -1.5 1.5 ]);
        xlabel('Tiempo (seconds)','FontName','Arial','FontSize', 12);
        ylabel('Amplitud','FontName','Arial','FontSize', 12);
        title('Puntos de Muestreo','FontName','Arial','FontSize', 16);
        legend('puntos');
       
           
    t=linspace(0,lim,1/0.00001)'; %generacion de vector de tiempo 
    yr=sinc((1/T)*t(:,ones(size(n)))-(1/T)*n(:,ones(size(t)))')*ys; %interpolacion de los puntos de muestreo
    
        %Grafica de Reconstruccion de la Señal
        subplot(212)
        plot(t,yr,'b');grid;
        axis([0 lim -1.5 1.5 ]);
        hold on
        stem(n,ys,'r');
        axis([0 lim -1.5 1.5 ]);
        hold on
        plot(ns,y,'g');
        axis([0 lim -1.5 1.5 ]);
        xlabel('Tiempo (seconds)','FontName','Arial','FontSize', 12);
        ylabel('Amplitud','FontName','Arial','FontSize', 12);
        title('Reconstruccion de la Señal','FontName','Arial','FontSize', 16);
        legend('Reconstruccion','Punto','Original');
            
%Implementacion para ver el espectro de Frecuencia
figure(2)

    t3=0:(1/s):(412/f(1)); %Extendemos el tiempo de observacion, Evaluamos 4 periodos de la señal que tal si ahora son 50 Periodos.
    %Evaluamos 824 puntos
    s3 = a(1)*sin( 2*f(1)*pi*t3 ); %Funcion Original
    frecu3=linspace(-s/2,s/2,length(s3));
    
            %Grafica de Funcion de Original
             subplot(211)
             hold on
                %%Propiedades de la gráfica
                xlabel('Freceuncia (Hz)','FontName','Arial','FontSize', 12);
                ylabel('Potencia','FontName','Arial','FontSize', 12);
                title('Espectro Frec.Señal Original','FontName','Arial','FontSize', 16);
                legend('real','splines');
                plot(frecu3,fftshift(abs(fft(s3))));grid
                
            
        ys=a(1)*sin(2*f(1)*pi*t3); %muestreo de la señal
        frecu3=linspace(-s/2,s/2,length(ys));
    
            %Grafica de Funcion de Original
            subplot(212)
             hold on
                %%Propiedades de la gráfica
                xlabel('Freceuncia (Hz)','FontName','Arial','FontSize', 12);
                ylabel('Potencia','FontName','Arial','FontSize', 12);
                title('Espectro Frec. Funcion Muestreo','FontName','Arial','FontSize', 16);
                legend('muestreo','splines');
                plot(frecu3,fftshift(abs(fft(ys))));grid
                
            
    
                
               
                ys=a(1)*sin(2*f(1)*pi*n); %muestreo de la señal
                
          
    figure(3)
         %Espectograma de  Funcion de Original
         frecu3=linspace(0,2*lim,length(yr));
            subplot(221)
             hold on
                %%Propiedades de la gráfica
                specgram(y);
                axis tight
                view(0,90)
                xlabel('Tiempo(s)','FontName','Arial','FontSize', 12);
                ylabel('Frequencia (Hz)','FontName','Arial','FontSize', 12);
                title('Espectograma Señal Original ','FontName','Arial','FontSize', 16);
               
                
                
         %Espectograma de Puntos de muestreo
            subplot(222)
            hold on
                %%Propiedades de la gráfica
                
                specgram(ys);
                axis tight
                view(0,90)
                xlabel('Tiempo(s)','FontName','Arial','FontSize', 12);
                ylabel('Frequencia (Hz)','FontName','Arial','FontSize', 12);
                title('Espectograma Puntos de Muestreo','FontName','Arial','FontSize', 16);
              



                
         %Espectograma de señal Recuperada
            subplot(212)
            %%Propiedades de la gráfica
            hold on
                specgram(yr);
                axis tight
                view(0,90)
                xlabel('Tiempo(s)','FontName','Arial','FontSize', 12);
                ylabel('Frequencia (Hz)','FontName','Arial','FontSize', 12);
                title('Espectograma Señal Reconstruida','FontName','Arial','FontSize', 16);
               
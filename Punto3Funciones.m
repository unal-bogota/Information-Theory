clc;
clear;
figure(1)
    f = [25 150 50];
    a = [3 10 -1];
    lim = 4/max(f); %lim del tiempo de graficacion
    t1 = 0:1*10^(-4):lim;
    y = a(1)*cos( 2*f(1)*pi*t1 ) + a(2)*sin(2*f(2)*pi*t1) + a(3)*cos(2*f(3)*pi*t1);
            %Grafica de Funcion de Original
            subplot(221)
            hold on
            grid on
                %%Propiedades de la gráfica
                xlabel('t (seconds)','FontName','Arial','FontSize', 12);
                ylabel('Amplitud','FontName','Arial','FontSize', 12);
                title('3*cos(50*pi*t)+10*sin(300*pi*t)-1*cos(100*pi*t)','FontName','Arial','FontSize', 16);
                legend('real');
                plot(t1,y,'g');
                axis([0 lim -(2*max(a)) (2*max(a)) ]);
                
               

    num=4;%la cantidad de frecuencia de muestre con num*frecuencia Analoga.
    s = num*max(f);
    T=1/s;
    
    n = (0:T:lim)';
    
    ys = a(1)*cos( 2*f(1)*pi*n ) + a(2)*sin(2*f(2)*pi*n) + a(3)*cos(2*f(3)*pi*n);
           %Grafica de Puntos Muestrales
            subplot(222)
            stem(n,ys,'or');grid
            axis([0 lim -(2*max(a)) (2*max(a)) ]);
            %Propiedades de la gráfica
            xlabel('Tiempo (seconds)','FontName','Arial','FontSize', 12);
            ylabel('Amplitud','FontName','Arial','FontSize', 12);
            title('Puntos de Muestreo','FontName','Arial','FontSize', 16);
            legend('puntos'); 
    
    t=linspace(0,lim,1/0.00001)'; %generacion de vector de tiempo 
    yr=sinc((1/T)*t(:,ones(size(n)))-(1/T)*n(:,ones(size(t)))')*ys; %interpolacion de los puntos de muestreo
    
           %Grafica de Puntos Muestrales, Recostruccion y Original
            subplot(212)
            plot(t,yr,'b');grid;
            axis([0 lim -(2*max(a)) (2*max(a)) ]);
            hold on
            stem(n,ys,'r');
            axis([0 lim -(2*max(a)) (2*max(a)) ]);
            hold on
            plot(t1,y,'g');
            axis([0 lim -(2*max(a)) (2*max(a)) ]);
            
            %Propiedades de la gráfica
            xlabel('Tiempo(seconds)','FontName','Arial','FontSize', 12);
            ylabel('Amplitud','FontName','Arial','FontSize', 12);
            title('Reconstruccion de la Señal','FontName','Arial','FontSize', 16);
            legend('Reconstruccion','Punto','Original');
            
%Implementacion para ver el espectro de Frecuencia
figure(2)

    t3=0:(1/s):(412/max(f)); %Extendemos el tiempo de observacion, Evaluamos 4 periodos de la señal que tal si ahora son 50 Periodos.
    %Evaluamos 824 puntos
    s3 = a(1)*cos( 2*f(1)*pi*t3 ) + a(2)*sin(2*f(2)*pi*t3) + a(3)*cos(2*f(3)*pi*t3); %Funcion Original
    frecu3=linspace(-s/2,s/2,length(s3));
    
            %Grafica de Funcion de Original
             subplot(211)
             hold on
                %%Propiedades de la gráfica
                xlabel('Freceuncia (Hz)','FontName','Arial','FontSize', 12);
                ylabel('Potencia','FontName','Arial','FontSize', 12);
                title('Espectro Frec.Señal Original','FontName','Arial','FontSize', 16);
                legend('real','splines');
                plot(frecu3,fftshift(abs(fft(s3))));grid
                
            
        %ys= a(1)*cos( 2*f(1)*pi*t3 ) + a(2)*sin(2*f(2)*pi*t3) + a(3)*cos(2*f(3)*pi*t3); %muestreo de la señal
        frecu3=linspace(-s/2,s/2,length(ys));
    
            %Grafica de Funcion de Original
            subplot(212)
             hold on
                %%Propiedades de la gráfica
                xlabel('Freceuncia (Hz)','FontName','Arial','FontSize', 12);
                ylabel('Potencia','FontName','Arial','FontSize', 12);
                title('Espectro Frec. Funcion Muestreo','FontName','Arial','FontSize', 16);
                legend('muestreo','splines');
                plot(frecu3,fftshift(abs(fft(ys))));grid
               

                
        ys=a(1)*cos(2*f(1)*pi*n); %muestreo de la señal
         
    figure(3)
         %Espectograma de  Funcion de Original
            subplot(221)
             hold on
                %%Propiedades de la gráfica
                specgram(y);
                axis tight
                view(0,90)
                xlabel('Tiempo(s)','FontName','Arial','FontSize', 12);
                ylabel('Frequencia (Hz)','FontName','Arial','FontSize', 12);
                title('Espectograma Señal Original ','FontName','Arial','FontSize', 16);
               
                
                
         %Espectograma de Puntos de muestreo
            subplot(222)
            hold on
                %%Propiedades de la gráfica
                
                specgram(ys);
                axis tight
                view(0,90)
                xlabel('Tiempo(s)','FontName','Arial','FontSize', 12);
                ylabel('Frequencia (Hz)','FontName','Arial','FontSize', 12);
                title('Espectograma Puntos de Muestreo','FontName','Arial','FontSize', 16);
              



                
         %Espectograma de señal Recuperada
            subplot(212)
            %%Propiedades de la gráfica
            hold on
                specgram(yr);
                axis tight
                view(0,90)
                xlabel('Tiempo(s)','FontName','Arial','FontSize', 12);
                ylabel('Frequencia (Hz)','FontName','Arial','FontSize', 12);
                title('Espectograma Señal Reconstruida','FontName','Arial','FontSize', 16);
               
        
      